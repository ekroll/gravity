extends Camera

onready var global = get_node("/root/Global")

var time = 0

func _ready():
	self.near = 0.001
	self.environment.fog_depth_begin = 0
	self.environment.fog_depth_end = global.view_distance
	self.environment.background_color = self.environment.fog_color
	self.environment.ambient_light_color = self.environment.fog_color
	self.environment.dof_blur_far_distance = 0
	self.environment.dof_blur_far_transition = global.view_distance
	self.environment.dof_blur_far_amount = 0.1
	self.far = global.view_distance

	self.environment.glow_enabled = true
	self.environment.ssao_enabled = false
	self.environment.fog_depth_enabled = true
	self.environment.adjustment_enabled = true
	self.environment.adjustment_enabled = true
	self.environment.fog_height_enabled = false
	self.environment.glow_bicubic_upscale = true
	self.environment.dof_blur_far_enabled = false
	self.environment.fog_transmit_enabled = false
	self.environment.dof_blur_near_enabled = false
	self.environment.auto_exposure_enabled = false
	self.environment.ss_reflections_enabled = true

func _process(delta):
	if self.get_owner().is_network_master():
		time += delta
		if self.get_owner().is_on_floor() and self.get_owner().is_moving:
			var h_offset = sin(time * self.get_owner().speed / 50) * global.magnitude(self.get_owner().velocity.normalized()) * global.magnitude(self.get_owner().scale) / 15
			var v_offset = sin(time * self.get_owner().speed / 25) * global.magnitude(self.get_owner().velocity.normalized()) * global.magnitude(self.get_owner().scale) / 15
			self.h_offset = global.interpolate(self.h_offset, h_offset, delta)
			self.v_offset = global.interpolate(self.v_offset, v_offset, delta)
