extends Spatial

var lifetime = 1
var speed = 50
var damage = 1
var direction
var shooter_id

onready var area = $Hitman
onready var Global = get_node("/root/Global")

func _ready():
	direction = self.global_transform.basis.z

func _process(delta):
	self.translation += direction * speed * delta
	#self.scale -= delta / lifetime
	if Global.magnitude(self.scale) < 0:
		self.queue_free()

func hit(poor_thing):
	if is_network_master():
		if not int(poor_thing.name) == int(shooter_id) and poor_thing.has_method("damage"):
			poor_thing.rpc_id(int(poor_thing.name), "damage", damage)
	self.queue_free()
