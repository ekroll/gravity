extends RayCast

onready var camera = $Camera

var cam_translation

func _process(delta):
	if self.is_colliding():
		cam_translation = self.get_collision_point()
		camera.global_transform.origin = camera.global_transform.origin.linear_interpolate(cam_translation, delta * 5)
	else:
		camera.translation = camera.translation.linear_interpolate(self.translation + Vector3(0, 0, 7), delta * 5)
