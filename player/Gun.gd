extends Spatial

var fire_rate = 0.075

onready var Paintball = load("res://player/gun/Paintball.tscn")
onready var muzzle = $mesh/muzzle

func fire():
	var paintball = Paintball.instance()
	paintball.transform = muzzle.global_transform
	get_node("/root").add_child(paintball)
