shader_type spatial;
render_mode unshaded;

vec2 scale_centered(vec2 uv, float factor) {
	return vec2((uv.x * factor) - (factor -1.0) / 2.0, (uv.y * factor) - (factor -1.0) / 2.0);
}

vec3 blur (sampler2D Texture, vec2 uv, vec2 pixel_size) {
	vec3 color = texture(Texture, uv, 0.0).rgb * 0.16;
	color += texture(Texture, uv + vec2(pixel_size.x, pixel_size.y)).xyz * 0.15;
	color += texture(Texture, uv + vec2(-pixel_size.x, -pixel_size.y)).xyz * 0.15;
	color += texture(Texture, uv + vec2(2.0 * pixel_size.x, 2.0 * pixel_size.y)).xyz * 0.12;
	color += texture(Texture, uv + vec2(2.0 * -pixel_size.x, 2.0 * -pixel_size.y)).xyz * 0.12;
	color += texture(Texture, uv + vec2(3.0 * pixel_size.x, 3.0 * pixel_size.y)).xyz * 0.09;
	color += texture(Texture, uv + vec2(3.0 * -pixel_size.x, 3.0 * -pixel_size.y)).xyz * 0.09;
	color += texture(Texture, uv + vec2(4.0 * pixel_size.x, 4.0 * pixel_size.y)).xyz * 0.05;
	color += texture(Texture, uv + vec2(4.0 * -pixel_size.x, 4.0 * -pixel_size.y)).xyz * 0.05;
	return color;
}

vec3 unreal(vec3 x) {
  return x / (x + 1.0) * 2.0;
}

void fragment() {
	float gradient = distance(SCREEN_UV, vec2(0.5)) * 2.0;
	vec2 uv = scale_centered(SCREEN_UV, 1.0 - gradient);
	vec3 focused = textureLod(SCREEN_TEXTURE, SCREEN_UV, 0.0).rgb;
	vec3 unfocused = blur(SCREEN_TEXTURE, SCREEN_UV, vec2(0.0015, 0.0015));
	
	ALBEDO = mix (focused, unfocused, gradient);
	ALBEDO *= 1.0 - distance(SCREEN_UV, vec2(0.5));
}