extends KinematicBody

# Network variables
puppet var puppet_translation = Vector3()
puppet var puppet_head_pivot_rotation = Vector3()
puppet var puppet_head_pivot_translation = Vector3()

# Player vars
onready var global = get_node("/root/Global")
onready var neck = $Neck
onready var head = $Neck/Head
onready var camera = $RayCast/Camera
onready var capsule = $CollisionShape
onready var body = $Body
onready var flashlight = $RayCast/Camera/FlashLight
onready var raycast = $RayCast
onready var animation = $Animator
onready var gun = $Neck/Gun

var pitch = 0
var speed = 0
var up = Vector3(0, 1, 0)
var movement = Vector3(0, 0, 0)
var velocity = Vector3(0, 0, 0)
var is_running = false
var is_moving = false
var can_jump = true
var inverted_gravity = false

#var agillity = 15

var jump_force = 10
var jump_height = 0.85

var sprint_speed = 750
var sprint_height = 1.55

var walk_speed = 300
var walk_height = 1.85

var crouch_speed = 150
var crouch_height = 1.0

var terminal_velocity = 63

func set_character_height(height, delta):
	capsule.translation.y = global.interpolate(capsule.shape.height, height - capsule.shape.radius * 2, delta * 15)
	capsule.shape.height = global.interpolate(capsule.shape.height, height - capsule.shape.radius * 2, delta * 15)
	capsule.shape.radius = head.mesh.size.y
	body.translation.y = capsule.translation.y
	body.mesh.mid_height = capsule.shape.height
	neck.translation.y = global.interpolate(neck.translation.y, capsule.shape.radius + capsule.shape.height, delta * 15)
	head.translation.y = global.interpolate(head.translation.y, capsule.shape.radius + head.mesh.size.y, delta)

func _ready():
	camera.make_current()
	#animation.current_animation = fancyrotation
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	flashlight.spot_range = global.view_distance
	flashlight.spot_angle = camera.fov / 2
	var peer = NetworkedMultiplayerENet.new()
	peer.create_server(1234, 1)
	get_tree().network_peer = peer


func _input(event):
	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		if not animation.is_playing():
			pitch = clamp(pitch - event.relative.y * global.mouse_sensitivity.y, -PI/2, PI/2)
		
		if inverted_gravity:
			neck.rotation.x = -pitch
		else:
			neck.rotation.x = pitch
		self.rotation.y -= event.relative.x * global.mouse_sensitivity.x
		
func _process(delta):
	if is_network_master():
		
		if Input.is_action_just_pressed("right_hand"):
			gun.fire()
		
		if inverted_gravity:
			if animation.is_playing():
				if animation.get_current_animation_position() > 0.5:
					animation.stop()
		else:
			if animation.is_playing():
				if animation.get_current_animation_position() > 0.99:
					animation.stop()
	
	#if animation.is_playing():
	raycast.rotation = global.interpolate(raycast.rotation, neck.rotation, delta / 1)
	#raycast.rotation = neck.rotation
	raycast.translation = neck.translation
	#if animation.current_animation_position > 4.9:
		#animation.stop()

func _physics_process(delta):
	if is_network_master():
		if Input.is_action_pressed("crouch"):
			if is_on_floor():
				speed = crouch_speed
			#set_character_height(crouch_height, delta)
		else:
			if Input.is_action_pressed("sprint"):
				speed = sprint_speed
					#set_character_height(sprint_height, delta)
			else:
				speed = walk_speed
		if Input.is_action_just_pressed("invert_gravity"):
			invert_gravity()
				
		
		is_moving = false
		var input_dir = Vector3()
		if Input.is_action_pressed("forward"):
			input_dir += Vector3(0, 0, -1).rotated(Vector3(0, 1, 0), self.rotation.y)
			is_moving = true
			
		elif Input.is_action_pressed("backward"):
			input_dir += Vector3(0, 0, 1).rotated(Vector3(0, 1, 0), self.rotation.y)
			is_moving = true
			
		if Input.is_action_pressed("left"):
			if inverted_gravity:
				input_dir += Vector3(1, 0, 0).rotated(Vector3(0, 1, 0), self.rotation.y)
			else:
				input_dir += Vector3(-1, 0, 0).rotated(Vector3(0, 1, 0), self.rotation.y)
			is_moving = true
			
		elif Input.is_action_pressed("right"):
			if inverted_gravity:
				input_dir += Vector3(-1, 0, 0).rotated(Vector3(0, 1, 0), self.rotation.y)
			else:
				input_dir += Vector3(1, 0, 0).rotated(Vector3(0, 1, 0), self.rotation.y)
			is_moving = true
		
		
		velocity.x = input_dir.normalized().x * speed * delta
		velocity.z = input_dir.normalized().z * speed * delta
		
		
		if is_on_floor() or is_on_wall():
			if Input.is_action_just_pressed("jump"):
				jump()
		
		if Input.is_action_pressed("jump"):
			velocity.y += jump_force * global.magnitude(self.scale) * delta * 2.75
		
		if Input.is_action_just_pressed("shrink"):
			if global.magnitude(self.scale) > 0.125:
				self.scale /= 5
		if Input.is_action_just_pressed("grow"):
			if global.magnitude(self.scale) < 1.0:
				self.scale *= 5
		
		if Input.is_action_just_pressed("ui_cancel"):
			self.translation = Vector3(0, 10, 7)
		
		velocity.x *= global.magnitude(self.scale)
		velocity.z *= global.magnitude(self.scale)
		velocity.y += global.gravity * delta * global.magnitude(self.scale)

		velocity = move_and_slide(velocity, up)


		# Inverts gravity and animates transition
		#if Input.is_action_just_pressed("invert_gravity"):
			#self.fix_inverted_gravity_stuff()
		#if inverse_gravity:
			#head_pivot.translation = head_pivot.translation + (head_pivot_loc_2 - head_pivot.translation) * delta * inv_gravity_interp_speed
			#head_pivot.rotation_degrees.z = head_pivot.rotation_degrees.z + (INV_GRAVITY_HEAD_ROT - head_pivot.rotation_degrees.z) * delta * inv_gravity_interp_speed
		#else:
			#head_pivot.translation = head_pivot.translation + (head_pivot_loc_1 - head_pivot.translation) * delta * inv_gravity_interp_speed
			#head_pivot.rotation_degrees.z = head_pivot.rotation_degrees.z + (NORMAL_HEAD_ROT - head_pivot.rotation_degrees.z) * delta * inv_gravity_interp_speed

# You got hit
#remote func hit(damage):
	#self.health -= damage
	#if self.health < 1:
		#self.die()

# Takes care of bad stuff when beeing upside down
func invert_gravity():
	if is_on_floor():
		jump()
	up *= -1
	jump_force = -jump_force
	global.mouse_sensitivity.x *= -1
	global.gravity *= -1
	if inverted_gravity:
		animation.seek(0.5)
	inverted_gravity = !inverted_gravity
	animation.play("InvertGravity")

func jump():
	velocity.y = jump_force * global.magnitude(self.scale) * 0.75

# You die
#func die():
	#health = max_health
	#self.translation = global.game.world.respawn_point


