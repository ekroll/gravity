extends Spatial

export var destination_name = "Destination Name"
export var destination = Vector3()
var is_return_portal = false

onready var Portal = load("res://world/Portal.tscn")
onready var Text3D = load("res://world/Text3D.tscn")
onready var global = get_node("/root/Global")

func _ready():
	var title = Text3D.instance()
	title.text = destination_name
	title.translation.y = 2.5
	title.translation.x = -1.25
	title.rotation_degrees.y = 90
	title.double_sided = false
	self.add_child(title)
	
	if not is_return_portal:
		var portal = Portal.instance()
		
		portal.destination_name = "Back"
		portal.destination = self.global_transform.origin
		portal.is_return_portal = true
		
		portal.translation = destination
		portal.rotation_degrees.y = self.rotation_degrees.y + 180
		portal.scale = self.scale
		self.get_parent().call_deferred("add_child", portal)
