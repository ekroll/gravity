extends Node

export var text = "Insert Text"
export var double_sided = true
export var rect_size = Vector2(250, 20)

onready var viewport = $Viewport
onready var sprite = $Sprite3D
onready var label = $Viewport/Label

func _ready():
	viewport.UPDATE_WHEN_VISIBLE
	viewport.size = rect_size
	label.rect_size = rect_size
	
	label.text = text
	sprite.texture = viewport.get_texture()
	sprite.double_sided = false
	
	if double_sided:
		var backside = Sprite3D.new()
		backside.texture = viewport.get_texture()
		backside.double_sided = false
		backside.flip_v = true
		backside.rotation_degrees.y += 180
		backside.scale = sprite.scale
		self.add_child(backside)
