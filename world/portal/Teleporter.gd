extends Area

func _on_body_entered(body):
	if get_owner().is_return_portal:
		body.translation = get_owner().destination
	else:
		body.translation = get_owner().destination + get_owner().global_transform.origin - get_owner().translation
