shader_type spatial;
render_mode cull_disabled;

varying vec3 not_normal;
varying vec3 vert;
uniform float speed_multiplier;
varying float time;

void vertex() {
	vert = VERTEX;
	time = TIME * speed_multiplier;
	vec3 original_vertex = VERTEX;
	VERTEX += 
	vec3(
		0,
		(
			sin(
				time + VERTEX.x * 16.0 
				+
				0.75 * time + VERTEX.x * 2.0
				+
				0.25 * time + VERTEX.x * 1.0
				+
				1.25 * time + VERTEX.x * 0.5
			) 
			/
			3.0
			+ 
			cos(
				TIME + VERTEX.z * 32.0
				+
				0.75 * time + VERTEX.z * 4.0
				+
				0.25 * time + VERTEX.z * 2.0
				+
				1.25 * time + VERTEX.z * 1.0
			)
		) / 2.0,
		sin(
			2.0 * TIME + VERTEX.x * 8.0
		) 
		/
		8.0
	);
	VERTEX.y *= 0.35;
	not_normal = VERTEX - original_vertex;
}

void fragment() {
	//ALBEDO = vert.yyy * 1.0;
	ALBEDO = vec3(0);
	//EMISSION = vec3(1.0, 1.0, 1.0) * (sin(time + ALBEDO) + 1.0) / 2.0;
	ALPHA = pow(1.0 - distance(vec2(0), vec2(vert.x * 2.0, vert.z)), 0.5);
}