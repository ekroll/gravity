extends Node

var mouse_sensitivity = Vector2(0.002, 0.002)
var view_distance = 100
var gravity = -30

func interpolate(a, b, delta):
	return a + (b - a) * 10 * delta

func magnitude(vector):
	var hyp_one = sqrt(pow(vector.x, 2) + pow(vector.z, 2))
	var length = sqrt(pow(hyp_one, 2) + pow(vector.y, 2))
	return length

func vec3avg(vector):
	return (vector.x + vector.y + vector.z) / 3

func _ready():
	OS.window_fullscreen = true

func _process(delta):
	if Input.is_action_just_pressed("ui_end"):
		get_tree().quit()
